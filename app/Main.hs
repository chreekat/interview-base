{-# LANGUAGE OverloadedStrings #-}

module Main where

import Web.Scotty
import Lucid

lucid :: Html () -> ActionM ()
lucid = html . renderText

main :: IO ()
main = scotty 3000 $
    get "/" $ do
        lucid $ doctypehtml_ $ do
            body_ $ do
                h1_ "Log search"
                p_ "Todo"
